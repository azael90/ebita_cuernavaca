package com.latinofuneral.bitacoras2020Cuernavaca.Callbacks;

public interface EditarDestinoBitacora {
    public void onClickEditarDestinoBitacora(int position, String bitacora);
}
