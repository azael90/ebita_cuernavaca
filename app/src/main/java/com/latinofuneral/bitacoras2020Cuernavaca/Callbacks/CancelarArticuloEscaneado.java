package com.latinofuneral.bitacoras2020Cuernavaca.Callbacks;

public interface CancelarArticuloEscaneado {
    public void onClickCancelarArticulo(int position, String id);
}
