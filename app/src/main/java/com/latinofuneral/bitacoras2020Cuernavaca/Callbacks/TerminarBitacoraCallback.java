package com.latinofuneral.bitacoras2020Cuernavaca.Callbacks;

public interface TerminarBitacoraCallback {
    public void onClickTerminarBitacora(int position, String bitacora);
}
