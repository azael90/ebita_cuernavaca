package com.latinofuneral.bitacoras2020Cuernavaca.Callbacks;

public interface RequerirEventoDeSalidaCallback {
    public void onRequeriedEventoSalida(int position, String bitacora);
}
