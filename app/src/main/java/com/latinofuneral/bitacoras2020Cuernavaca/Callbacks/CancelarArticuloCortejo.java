package com.latinofuneral.bitacoras2020Cuernavaca.Callbacks;

public interface CancelarArticuloCortejo {
    public void onClickCancelarArticuloCortejo(int position, String serie, String fecha, String bitacora);
}
