package com.latinofuneral.bitacoras2020Cuernavaca.Callbacks;

public interface CancelarArticuloInstalacion {
    public void onClickCancelarArticuloInstalacion(int position, String serie, String fecha, String bitacora);
}
