package com.latinofuneral.bitacoras2020Cuernavaca.Callbacks;

public interface ShowDetailsBitacoraCallback {
    public void onClickShowDetails(int position, String bitacora);
}
