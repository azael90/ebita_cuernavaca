package com.latinofuneral.bitacoras2020Cuernavaca.Callbacks;

public interface RegistrarSalidaCallback {
    public void onClickRegistrarSalida(int position, String bitacora, String destino);
}
