package com.latinofuneral.bitacoras2020Cuernavaca.Utils;


@SuppressWarnings("FieldCanBeLocal")
public final class BuildConfig {

    private static Branches targetBranch = Branches.GUADALAJARA;

    @SuppressWarnings("SpellCheckingInspection")
    public static enum Branches{GUADALAJARA, CIUDAD}

    private BuildConfig(){}

    public static Branches getTargetBranch(){
        return targetBranch;
    }

}
