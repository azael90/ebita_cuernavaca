package com.latinofuneral.bitacoras2020Cuernavaca.Adapters;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.latinofuneral.bitacoras2020Cuernavaca.Activities.Login;
import com.latinofuneral.bitacoras2020Cuernavaca.Callbacks.CancelarArticuloEscaneado;
import com.latinofuneral.bitacoras2020Cuernavaca.Models.ModelArticulosEscaneados;
import com.latinofuneral.bitacoras2020Cuernavaca.R;

import java.util.List;

import soup.neumorphism.NeumorphButton;
import soup.neumorphism.NeumorphCardView;

public class AdapterLoginOpciones extends RecyclerView.Adapter<AdapterLoginOpciones.OptionsViewHolder> {
    private static final String TAG = "AdapterOptions";
    private Login mCtx;
    private List<String> options;


    public AdapterLoginOpciones(Login mCtx, List<String> options) {
        this.mCtx = mCtx;
        this.options = options;
    }

    @Override
    public OptionsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.item_login_opciones, null);
        OptionsViewHolder holder = new OptionsViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(OptionsViewHolder holder, int position)
    {

        holder.tvOption.setText(options.get(position));
        holder.cardOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCtx.optionSelected = position;
                notifyDataSetChanged();
            }
        });

        if (position == 0)
            holder.imgOption.setImageDrawable(mCtx.getResources().getDrawable(R.drawable.ic_car, mCtx.getApplicationContext().getTheme()));
        if (position == 1)
            holder.imgOption.setImageDrawable(mCtx.getResources().getDrawable(R.drawable.ic_person, mCtx.getApplicationContext().getTheme()));
        if (position == 2)
            holder.imgOption.setImageDrawable(mCtx.getResources().getDrawable(R.drawable.ataud, mCtx.getApplicationContext().getTheme()));
        if (position == 3)
            holder.imgOption.setImageDrawable(mCtx.getResources().getDrawable(R.drawable.ic_cremacion, mCtx.getApplicationContext().getTheme()));
        if (position == 4)
            holder.imgOption.setImageDrawable(mCtx.getResources().getDrawable(R.drawable.ic_asistencia, mCtx.getApplicationContext().getTheme()));

        if (mCtx.optionSelected == position)
          holder.cardOption.setBackgroundColor(mCtx.getResources().getColor(R.color.textos_general));
        else
            holder.cardOption.setBackgroundColor(mCtx.getResources().getColor(R.color.white));

    }





    @Override
    public int getItemCount() {
        return options.size();
    }

    class OptionsViewHolder extends RecyclerView.ViewHolder
    {
        NeumorphCardView cardOption;
        ImageView imgOption;
        TextView tvOption;
        public OptionsViewHolder(View itemView)
        {
            super(itemView);
            cardOption= itemView.findViewById(R.id.cardOption);
            imgOption= itemView.findViewById(R.id.imgOption);
            tvOption= itemView.findViewById(R.id.tvOption);

        }
    }


}
